# HapiVMM

La police de Boston a besoin d’un nouvel outil d’exploration et recensement des crimes. Votre travail est de le créer !
Pour ce faire vous avez à votre disposition un export de la base de données de la police sous format CSV.
Le chef de la police vous demande explicitement de :
Fournir des outils de visualisation des données (crimes par date, par type, par localisation, ...)
Forcer l’identification de chaque utilisateur, et leur attribuer des droits différents selon leurs grades. Agent (consultation), Détective (consultation, ajout, et modification), Chef de la police (consultation, ajout, modification et suppression)
Chaque nouveau compte devra être validé par le Chef de la police.

. Un front avec VueJS / Vuex / Nuxt
. Une API en HapiJS
. Une base de données Mongo & MySQL
. Lumen (pour vos microservices)