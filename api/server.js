'use strict';

const Hapi = require('hapi');
const axios = require('axios');
const Boom = require('boom');

const server = new Hapi.Server({
    port: 3000,
    host: '0.0.0.0',
    routes: {
        "cors": true
    }
});

server.route({
    method: 'GET',
    path: '/exportToCsv',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.get(
                "http://exportuserservice/exportToCsv", {
                headers: {
                    Authorization: tokenValue
                }
            });
        } catch (error) {
            if (error.response){
                return Boom.boomify(new Error(error.response.data.message), {statusCode: error.response.status});
            } else {
                return Boom.boomify(new Error('Oups'), {statusCode: 500});
            }
        }
        return h.response(response.data).header('Content-Type', 'text/csv').header('Content-Disposition', 'attachment; filename=users.csv');
    }
});

server.route({
    method: 'GET',
    path: '/users',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.get(
                "http://userservice/users", {
                    headers: {
                        Authorization: tokenValue
                    }
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'GET',
    path: '/users/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.get(
                "http://userservice/users/" + request.params.id, {
                    headers: {
                        Authorization: tokenValue
                    }
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'PUT',
    path: '/users/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.put(
                "http://userservice/users/" + request.params.id, request.payload, {
                    headers: {
                        Authorization: tokenValue
                    },
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'DELETE',
    path: '/users/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.delete(
                "http://userservice/users/" + request.params.id, {
                    headers: {
                        Authorization: tokenValue
                    },
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'GET',
    path: '/login',
    handler: async (request, h) => {
        let response = {};
        try {
            var queryString = Object.keys(request.query).map(key => key + '=' + request.query[key]).join('&');
            response = await axios.get(
                "http://loginservice/login?" + queryString);
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'GET',
    path: '/logout',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.get(
                "http://loginservice/logout", {
                    headers: {
                        Authorization: tokenValue
                    }
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), { statusCode: error.response.status });
            } else {
                return Boom.boomify(new Error('Oups'), { statusCode: 500 });
            }
        }
        return h.response(response.data).header('Content-Type', 'text/csv').header('Content-Disposition', 'attachment; filename=users.csv');
    }
});


server.route({
    method: 'POST',
    path: '/register',
    handler: async (request, h) => {
        let response = {};
        try {
            response = await axios.post(
                "http://registerservice/register", request.payload);
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(JSON.stringify(error.response.data)), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'GET',
    path: '/crimes',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            var queryString = Object.keys(request.query).map(key => key + '=' + request.query[key]).join('&');
            response = await axios.get(
                "http://searchservice/crimes?" + queryString, {
                    headers: {
                        Authorization: tokenValue
                    }
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'GET',
    path: '/crimes/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.get(
                "http://searchservice/crimes/" + request.params.id, {
                    headers: {
                        Authorization: tokenValue
                    }
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'PUT',
    path: '/crimes/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.put(
                "http://searchservice/crimes/" + request.params.id, request.payload, {
                    headers: {
                        Authorization: tokenValue
                    },
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'DELETE',
    path: '/crimes/{id}',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.delete(
                "http://searchservice/crimes/" + request.params.id, {
                    headers: {
                        Authorization: tokenValue
                    },
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(error.response.data.message), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

server.route({
    method: 'POST',
    path: '/crimes',
    handler: async (request, h) => {
        let response = {};
        try {
            if (request.headers.authorization === undefined) {
                var tokenValue = "Bearer undefined";
            } else {
                var tokenValue = request.headers.authorization;
            }
            response = await axios.post(
                "http://searchservice/crimes", request.payload, {
                    headers: {
                        Authorization: tokenValue
                    },
                });
        } catch (error) {
            if (error.response) {
                return Boom.boomify(new Error(JSON.stringify(error.response.data)), {
                    statusCode: error.response.status
                });
            } else {
                return Boom.boomify(new Error('Oups'), {
                    statusCode: 500
                });
            }
        }
        return h.response(response.data).header('Content-Type', 'application/json');
    }
});

(async () => {
    try {
        await server.start();
        console.log(`Server running at: ${server.info.uri}`);
    } catch (err) {
        console.log(err)
    }
})();