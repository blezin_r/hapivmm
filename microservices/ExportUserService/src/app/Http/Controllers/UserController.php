<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function exportToCsv()
    {
        $users = \App\User::all();

        $entitiesArray = $users->toArray();

        $filename = "users.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array_keys($entitiesArray[0]));
        foreach ($entitiesArray as $line) {
            fputcsv($handle, $line);
        }

        $headers = [
            'Content-Type' => 'text/csv',
        ];
        return response()->download($filename, 'users.csv', $headers);
    }
}
