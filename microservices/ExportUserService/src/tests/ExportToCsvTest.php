<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExportToCsvTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExportToCsv()
    {
        $this->get('/exportToCsv');

        $this->assertNotNull(
            $this->response->getContent()
        );
    }
}
