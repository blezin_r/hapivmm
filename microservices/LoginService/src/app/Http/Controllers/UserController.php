<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        //  $this->middleware('auth:api');
    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function authenticate(Request $request)
    {
        $this->validate($request, [

       'email' => 'required',

       'password' => 'required'

        ]);

        $user = User::where('email', $request->input('email'))->first();

        if ($user !== null && Hash::check($request->input('password'), $user->password) && $user->role = "admin" && $user->role > 0) {
            $apikey = base64_encode(str_random(40));

            User::where('email', $request->input('email'))->update(['api_key' => "$apikey"]);
            return response()->json(['status' => 'success','api_key' => $apikey]);
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }
    public function logout(Request $request)
    {
        $User = User::findOrFail($request['userid']);
        $User->api_key = null;
        $User->save();

        return response()->json($User, 200);
    }
}
