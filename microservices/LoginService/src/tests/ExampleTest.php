<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $this->get('/login?email=admin@admin.com&password=admin');
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
                [
                    'status',
                    'api_key',
                ]
        );
    }
    public function testLoginFailed()
    {
        $this->get('/login?email=admin@admin.com&password=admi');
        $this->seeStatusCode(401);
        $this->seeJsonStructure(
                [
                    'status',
                ]
        );
    }
}
