<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->where('name', '=', 'admin')->first() === null) {
            DB::table('users')->insert([
            'name' => "admin",
            'email' => "admin".'@admin.com',
            'role' => 3,
            'password' => app('hash')->make('admin'),
        ]);
        }
    }
}
