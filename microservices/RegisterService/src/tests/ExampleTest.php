<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    public function testRegister()
    {
        $parameters = [
            'name' => 'test test',
            'email' => 'test@test.com',
            'password' => 'test@test.com',
            'password_confirmation' => 'test@test.com',
        ];
        $this->post("register", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    'id'
                ]
            ]
        );
    }
    public function testRegisterFailed()
    {
        $parameters = [
            'name' => 'test test',
            'email' => 'test@test.com',
            'password' => '123',
            'password_confirmation' => '123',
        ];
        $this->post("register", $parameters, []);
        $this->seeStatusCode(422);
    }
}
