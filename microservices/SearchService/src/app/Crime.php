<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Crime extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'crimes';
    public $timestamps = false;

    protected $fillable = [
        "_id", "compnos", "naturecode",
        "incident_type_description", "main_crimecode",
        "reptdistrict", "reportingarea", "fromdate",
        "weapontype", "shooting", "domestic", "shift",
        "year", "month", "day_week", "ucrpart", "x", "y",
        "streetname", "xstreetname", "location"
    ];
}
