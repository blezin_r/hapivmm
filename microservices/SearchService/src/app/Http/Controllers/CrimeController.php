<?php

namespace App\Http\Controllers;

use App\Crime;
use Illuminate\Http\Request;

class CrimeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAllCrimes(Request $request)
    {
        $limit  = (int) $request->get('limit');
        $offset = (int) $request->get('offset');

        $compnos                   = $request->get('compnos');
        $id                        = $request->get('id');
        $reportingarea             = $request->get('reportingarea');
        $reptdistrict              = $request->get('reptdistrict');
        $naturecode                = $request->get('naturecode');
        $main_crimecode            = $request->get('main_crimecode');
        $fromdate                  = $request->get('fromdate');
        $weapontype                = $request->get('weapontype');
        $shooting                  = $request->get('shooting');
        $domestic                  = $request->get('domestic');
        $shift                     = $request->get('shift');
        $year                      = $request->get('year');
        $month                     = $request->get('month');
        $day_week                  = $request->get('day_week');
        $ucrpart                   = $request->get('ucrpart');
        $x                         = $request->get('x');
        $y                         = $request->get('y');
        $streetname                = $request->get('streetname');
        $xstreetname               = $request->get('xstreetname');
        $location                  = $request->get('location');

        $crimes = Crime::where('1 = 1');
        $model = new Crime();
        $field = $model->getFillable();

        // foreach ($request->all() as $paramKey => $paramValue) {
        //     if (in_array($paramKey, $field)) {
        //         $crimes->where($paramKey, '=', $paramValue);
        //     }
        // }
        if ($id !== null && $id != "") {
            $crimes->where('_id', '=', $id);
        }
        if ($compnos !== null && $compnos != "") {
            $crimes->where('compnos', '=', $compnos);
        }
        if ($reportingarea !== null && $reportingarea != "") {
            $crimes->where('reportingarea', '=', $reportingarea);
        }
        if ($reptdistrict !== null && $reptdistrict != "") {
            $crimes->where('reptdistrict', '=', $reptdistrict);
        }
        if ($naturecode !== null && $naturecode != "") {
            $crimes->where('naturecode', '=', $naturecode);
        }
        if ($main_crimecode !== null && $main_crimecode != "") {
            $crimes->where('main_crimecode', '=', $main_crimecode);
        }
        if ($fromdate !== null && $fromdate != "") {
            $crimes->where('fromdate', 'like', '%' . $fromdate . '%');
        }
        if ($weapontype !== null && $weapontype != "") {
            $crimes->where('weapontype', '=', $weapontype);
        }
        if ($shooting !== null && $shooting != "") {
            $crimes->where('shooting', '=', $shooting);
        }
        if ($domestic !== null && $domestic != "") {
            $crimes->where('domestic', '=', $domestic);
        }
        if ($shift !== null && $shift != "") {
            $crimes->where('shift', '=', $shift);
        }
        if ($year !== null && $year != "") {
            $crimes->where('year', '=', (int) $year);
        }
        if ($month !== null && $month != "") {
            $crimes->where('month', '=', (int) $month);
        }
        if ($day_week !== null && $day_week != "") {
            $crimes->where('day_week', '=', $day_week);
        }
        if ($ucrpart !== null && $ucrpart != "") {
            $crimes->where('ucrpart', '=', $ucrpart);
        }
        if ($x !== null && $x != "") {
            $crimes->where('x', '=', $x);
        }
        if ($y !== null && $y != "") {
            $crimes->where('y', '=', $y);
        }
        if ($streetname !== null && $streetname != "") {
            $crimes->where('streetname', 'like', '%' . $streetname . '%');
        }
        if ($xstreetname !== null && $xstreetname != "") {
            $crimes->where('xstreetname', '=', $xstreetname);
        }
        if ($location !== null && $location != "") {
            $crimes->where('location', '=', $location);
        }

        if ($offset !== null && is_numeric($offset)) {
            $crimes->offset($offset);
        }
        if ($limit !== null && is_numeric($limit)) {
            $crimes->limit($limit);
        }


        return response()->json($crimes->get());
    }

    public function showOneCrime($id)
    {
        return response()->json(Crime::find($id));
    }

    public function update($id, Request $request)
    {
        $crime = Crime::findOrFail($id);
        $this->validate($request, [
            "compnos" => 'integer|required',
            "reportingarea" => 'nullable|',
            "incident_type_description" => 'nullable|string',
            "reptdistrict" => 'string|nullable',
            "naturecode" => 'nullable',
            "main_crimecode" => 'nullable|string|',
            "fromdate" => 'nullable|date',
            "weapontype" => 'string|nullable|',
            "shooting" => 'nullable',
            "domestic" => 'string|nullable|',
            "shift" => 'string|nullable|',
            "year" => 'nullable',
            "month" => 'nullable',
            "day_week" => 'string|nullable|',
            "ucrpart" => 'string|nullable|',
            "x" => 'numeric|nullable',
            "y" => 'numeric|nullable',
            "streetname" => 'nullable',
            "xstreetname" => 'nullable',
            "location" => 'nullable'
        ]);

        $crime->update($request->all());
        return response()->json($crime, 200);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            "compnos" => 'integer|required',
            "reportingarea" => 'nullable|',
            "incident_type_description" => 'nullable|string',
            "reptdistrict" => 'string|nullable',
            "naturecode" => 'nullable',
            "main_crimecode" => 'nullable|string|',
            "fromdate" => 'nullable|date',
            "weapontype" => 'string|nullable|',
            "shooting" => 'nullable',
            "domestic" => 'string|nullable|',
            "shift" => 'string|nullable|',
            "year" => 'nullable',
            "month" => 'nullable',
            "day_week" => 'string|nullable|',
            "ucrpart" => 'string|nullable|',
            "x" => 'numeric|nullable',
            "y" => 'numeric|nullable',
            "streetname" => 'nullable',
            "xstreetname" => 'nullable',
            "location" => 'nullable'
        ]);

        if (Crime::create($request->all())) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function delete($id)
    {
        Crime::findOrFail($id)->delete();
        return response()->json(["message" => "Deleted Successfully"], 200);
    }
}
