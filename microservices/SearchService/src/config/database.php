<?php
return [

    'default' => 'mysql',

    'connections' => [

        'mongodb' => [
            'driver'   => 'mongodb',
            'host'     => env('DBM_HOST', 'localhost'),
            'port'     => env('DBM_PORT', 27017),
            'database' => env('DBM_DATABASE'),
            'username' => env('DBM_USERNAME'),
            'password' => env('DBM_PASSWORD'),
            'options' => [
                'database' => 'admin' // sets the authentication database required by mongo 3
            ]
        ],
        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],


    ],

    'migrations' => 'migrations',
];
