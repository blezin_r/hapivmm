<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('crimes', ['uses' => 'CrimeController@showAllCrimes']);
$router->get('crimes/{id}', ['uses' => 'CrimeController@showOneCrime']);
$router->post('crimes', ['middleware' => 'detective', 'uses' => 'CrimeController@store']);
$router->delete('crimes/{id}', ['middleware' => 'admin', 'uses' => 'CrimeController@delete']);
$router->put('crimes/{id}', ['middleware' => 'detective', 'uses' => 'CrimeController@update']);
