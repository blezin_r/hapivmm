<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    public function testcget()
    {
        $this->get("crimes?limit=1");

        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['*' =>
                [
                    "compnos",
                    "reportingarea",
                    "incident_type_description",
                    "reptdistrict",
                    "naturecode",
                    "main_crimecode",
                    "fromdate",
                    "weapontype",
                    "shooting",
                    "domestic",
                    "shift",
                    "year",
                    "month",
                    "day_week",
                    "ucrpart",
                    "x",
                    "y",
                    "streetname",
                    "xstreetname",
                    "location"
                ]
        ]
        );
    }

    public function testgetCrime()
    {
        $this->get("crimes/5bbf1c676d420b813daa6bea");
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
             [
                    "_id",
                    "compnos",
                    "reportingarea",
                    "incident_type_description",
                    "reptdistrict",
                    "naturecode",
                    "main_crimecode",
                    "fromdate",
                    "weapontype",
                    "shooting",
                    "domestic",
                    "shift",
                    "year",
                    "month",
                    "day_week",
                    "ucrpart",
                    "x",
                    "y",
                    "streetname",
                    "xstreetname",
                    "location"
                ]
        );
    }
    // public function testShouldDelete()
    // {
    //     $this->delete("users/1", [], []);
    //     $this->seeStatusCode(200);
    //     $this->seeJsonStructure([
    //             'message'
    //     ]);
    // }
}
