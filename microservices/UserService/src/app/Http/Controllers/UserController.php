<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function showAllUsers()
    {
        return response()->json(User::all());
    }

    public function showOneUser($id)
    {
        return response()->json(User::find($id));
    }

    public function update($id, Request $request)
    {
        $User = User::findOrFail($id);
        if ($request->get('role') !== null) {
            $User->role = $request->get('role');
            if ($User->role == 0) {
                $User->api_key = null;
            }
        }
        $User->update($request->all());

        return response()->json($User, 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response()->json(["message" => "Deleted Successfully"], 200);
    }
}
