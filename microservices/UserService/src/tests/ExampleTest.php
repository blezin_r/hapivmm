<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    public function testcget()
    {
        $this->get("users");

        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['*' =>
                [
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    'id',
                ]
        ]
        );
    }
    public function testgetUser()
    {
        $this->get("users/1");
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
                [
                    'id',
                    'name',
                    'email',
                    'role',
                    'created_at',
                    'updated_at'

                ]
        );
    }
    public function testShouldDelete()
    {
        $this->delete("users/1", [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
                'message'
        ]);
    }
}
