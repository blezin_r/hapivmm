module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'vue-hmmv',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'etna&apos;s project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    '~assets/vendor/bootstrap/css/bootstrap.min.css',
    '~assets/vendor/fontawesome-free/css/all.min.css',
    '~assets/css/sb-admin.css'
  ],
  modules: [
      'bootstrap-vue/nuxt',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: ['axios', 'vue2-google-maps']
  },
  plugins: [
    { src: '~/plugins/vue2-google-maps.js', ssr: false }
  ]
}