import Vuex from 'vuex'
import Cookie from 'js-cookie'

var cookieparser = require('cookieparser')

const createStore = () => {
  return new Vuex.Store({
    state: {
      breadcrumbsPage: '',
      auth: null,
      userNumber: 0,
      idUser: 0,
      idCrime: 0
    },
    mutations: {
      increment (state, title) {
        state.breadcrumbsPage = title
      },
      update (state, data) {
        state.auth = data
      },
      logout(state){
        this.commit('update', null)
      },
      setIdUser(state, id){
        state.idUser = id
      },
      setIdCrime(state, id){
        state.idCrime = id
      }
    },
    actions: {
      nuxtServerInit ({ commit }, { req }) {
        let accessToken = null
        if (req.headers.cookie) {
          var parsed = cookieparser.parse(req.headers.cookie)
          accessToken = JSON.parse(parsed.auth)
        }
        commit('update', accessToken)
      }
    }
  })
}

export default createStore